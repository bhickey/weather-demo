//
//  WeatherTableViewCell.swift
//  Weather
//
//  Created by brian hickey on 02/04/2019.
//  Copyright © 2019 brian hickey. All rights reserved.
//

import Foundation
import UIKit

class WeatherTableViewCell : UITableViewCell
{
    var titleLabel: UILabel = UILabel()
    var tempLabel: UILabel = UILabel()
    var windLabel: UILabel = UILabel()
    var dateLabel: UILabel = UILabel()
    var imgView: UIImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imgView.backgroundColor = .clear
        imgView.contentMode = .scaleAspectFit
        self.contentView.addSubview(imgView)
        
        tempLabel.backgroundColor = .clear
        tempLabel.font = Constants.appFontWithSize(size: 18)
        tempLabel.adjustsFontSizeToFitWidth = true
        tempLabel.textAlignment = .center
        tempLabel.textColor = .black
        tempLabel.textAlignment = .right
        self.contentView.addSubview(tempLabel)
        
        titleLabel.backgroundColor = .clear
        titleLabel.font = Constants.appFontBoldWithSize(size: 16)
        titleLabel.textAlignment = .center
        titleLabel.textColor = .black
        self.contentView.addSubview(titleLabel)
        
        
        windLabel.backgroundColor = .clear
        windLabel.font = Constants.appFontWithSize(size: 14)
        windLabel.adjustsFontSizeToFitWidth = true
        windLabel.textAlignment = .center
        windLabel.textColor = .black
        self.contentView.addSubview(windLabel)
        
        
        
        
        dateLabel.backgroundColor = .clear
        dateLabel.font = Constants.appFontMediumWithSize(size: 12)
        dateLabel.textColor = .black
        dateLabel.text = ""
        dateLabel.textColor = Constants.grey
        dateLabel.textAlignment = .center
        self.contentView.addSubview(dateLabel)
        
        setupConstraints()
    }
    
    
    
    func setupConstraints()
    {
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        tempLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        imgView.translatesAutoresizingMaskIntoConstraints = false
        windLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addConstraints([
            
            
            
            NSLayoutConstraint(item: imgView, attribute: .top, relatedBy: .equal,
                               toItem: contentView, attribute: .top,
                               multiplier: 1, constant: Constants.margin),
            NSLayoutConstraint(item: imgView, attribute: .left, relatedBy: .equal,
                               toItem: contentView, attribute: .left,
                               multiplier: 1, constant: Constants.margin),
            NSLayoutConstraint(item: imgView, attribute: .bottom, relatedBy: .equal,
                               toItem: contentView, attribute: .bottom,
                               multiplier: 1,constant: -Constants.margin),
            NSLayoutConstraint(item: imgView, attribute: .width, relatedBy: .equal,
                               toItem: imgView, attribute: .height,
                               multiplier: 1,constant: 0),
            
            
            NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal,
                               toItem: dateLabel, attribute: .bottom,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal,
                               toItem: imgView, attribute: .right,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal,
                               toItem: tempLabel, attribute: .left,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: titleLabel, attribute: .bottom, relatedBy: .equal,
                               toItem: windLabel, attribute: .top,
                               multiplier: 1,constant: 0),
            
            
            NSLayoutConstraint(item: windLabel, attribute: .top, relatedBy: .equal,
                               toItem: titleLabel, attribute: .bottom,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: windLabel, attribute: .left, relatedBy: .equal,
                               toItem: titleLabel, attribute: .left,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: windLabel, attribute: .right, relatedBy: .equal,
                               toItem: titleLabel, attribute: .right,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: windLabel, attribute: .bottom, relatedBy: .equal,
                               toItem: self.contentView, attribute: .bottom,
                               multiplier: 1,constant: -Constants.margin),
            
            NSLayoutConstraint(item: tempLabel, attribute: .top, relatedBy: .equal,
                               toItem: self.contentView, attribute: .top,
                               multiplier: 1, constant: Constants.margin),
            NSLayoutConstraint(item: tempLabel, attribute: .width, relatedBy: .equal,
                               toItem: tempLabel, attribute: .height,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tempLabel, attribute: .right, relatedBy: .equal,
                               toItem: self.contentView, attribute: .right,
                               multiplier: 1, constant: -Constants.margin),
            NSLayoutConstraint(item: tempLabel, attribute: .bottom, relatedBy: .equal,
                               toItem: self.contentView, attribute: .bottom,
                               multiplier: 1,constant: -Constants.margin),
            
        
            
            
            NSLayoutConstraint(item: dateLabel, attribute: .top, relatedBy: .equal,
                               toItem: self.contentView, attribute: .top,
                               multiplier: 1, constant: 0),
            NSLayoutConstraint(item: dateLabel, attribute: .left, relatedBy: .equal,
                               toItem: titleLabel, attribute: .left,
                               multiplier: 1, constant: Constants.margin),
            NSLayoutConstraint(item: dateLabel, attribute: .right, relatedBy: .equal,
                               toItem: titleLabel, attribute: .right,
                               multiplier: 1, constant: -Constants.margin),
            NSLayoutConstraint(item: dateLabel, attribute: .height, relatedBy: .equal,
                               toItem: nil, attribute: .notAnAttribute,
                               multiplier: 1,constant: Constants.margin)
            
            
            
            ])
        
        
    }
    
    public func clean()
    {
        titleLabel.text = ""
        windLabel.text = ""
        tempLabel.text = ""
        imgView.image = UIImage()
        dateLabel.text = ""
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
}
