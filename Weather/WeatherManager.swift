//
//  WeatherManager.swift
//  Weather
//
//  Created by brian hickey on 02/04/2019.
//  Copyright © 2019 brian hickey. All rights reserved.
//

import Foundation
import UIKit
typealias JSON = [String: Any]

enum WeatherMode : String
{
    case current
    case saved
}

enum Keys : String
{
    case mode
    case location
}

struct Weather : Codable
{
    var dateString : String?
    var unixDate : Int?
    var text : String?
    var icon : String?
    var temp : Double?
    var windSpeed : Double?
    //var
}

extension Weather {
    init?(json: JSON) {
        
        if let value = json["dt"] as? Int
        {
            self.unixDate = value
        }
        
        if let value = json["dt_txt"] as? String
        {
            self.dateString = value
        }
        
        if let weathers = json["weather"] as? [JSON]
        {
            if weathers.count > 0
            {
                let weather = weathers[0] as JSON
                
                if let value = weather["main"] as? String
                {
                    self.text = value
                }
                if let value = weather["icon"] as? String
                {
                    self.icon = value
                }
            }
        }
        if let main = json["main"] as? JSON
        {
            if let value = main["temp"] as? Double
            {
                self.temp = value - 273.15//kelvin correction
            }
        }
        if let wind = json["wind"] as? JSON
        {
            if let value = wind["speed"] as? Double
            {
                self.windSpeed = value
            }
        }
    }
}

struct Location : Codable
{
    var title : String?
    var lat : Double?
    var lon : Double?
}


class WeatherManager : NSObject
{
    static let shared = WeatherManager()
    public var weathers = [Weather]()
    
    public var currentLocation : Location?
    {
        guard let mode = currentMode else
        {
            return nil
        }
        
        switch mode
        {
            case WeatherMode.current:
                LocationManager.shared.permissionRequest()
                return LocationManager.shared.currentLocation
            case WeatherMode.saved:
                return currentSavedLocation
        }
    }
    
    
    public var currentMode : WeatherMode?
    {
        if let mode = UserDefaults.standard.string(forKey: Keys.mode.rawValue)
        {
            return WeatherMode(rawValue: mode)
        }
        return nil
    }
    
    func saveMode(weatherMode : WeatherMode)
    {
        UserDefaults.standard.set(weatherMode.rawValue, forKey: Keys.mode.rawValue)
        NotificationCenter.default.post(name: Notification.Name(Keys.mode.rawValue), object: nil, userInfo: nil)
        self.loadWeather()
    }
    
    
    public var currentSavedLocation : Location?
    {
        guard let data = UserDefaults.standard.value(forKey:Keys.location.rawValue) as? Data else {
            return nil
        }
        guard let location = try? PropertyListDecoder().decode(Location.self, from: data) else {
            
            return nil
        }
        
        
        return location
    }
    
    
    func saveLocation(location : Location)
    {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(location), forKey:Keys.location.rawValue)
        NotificationCenter.default.post(name: Notification.Name(Keys.location.rawValue), object: nil, userInfo: nil)
        
    }
    
    
    
    func loadWeather()
    {
        
        guard let lat = currentLocation?.lat, let lon = currentLocation?.lon else {
            
            return
        }
        
        
        var urlString = "https://api.openweathermap.org/data/2.5/forecast?"
        
        let latString = String(format: "&lat=%.5f", lat)
        urlString.append(latString)
        
        let lonString = String(format: "&lon=%.5f", lon)
        urlString.append(lonString)
        
        let key = "&APPID=0a45921589d7e8b0b022f2f5f3cae90e"
        urlString.append(key)
        
        
        guard let url = URL(string: urlString) else {
            
            return
        }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"
        let session = URLSession(configuration: .default)
        let task : URLSessionDataTask = session.dataTask(with: request) { (data, response, error) in
         
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200{
                var object: Any? = nil
                if let data = data {
                    object = try? JSONSerialization.jsonObject(with: data, options: [])
                    
                    
                    if let json = object as? JSON
                    {
                        if let list = json["list"] as? [JSON]
                        {
                            var temp = [Weather]()
                            
                            for weatherJSON in list
                            {
                                if let weather = Weather(json: weatherJSON)
                                {
                                    temp.append(weather)
                                }
                            }
                            
                            self.weathers = temp
                        }
                    }
                    
                    NotificationCenter.default.post(name: Notification.Name("refresh"), object: nil, userInfo: nil)
                    
                }
                else
                {
                    
                }
            }
        }
        task.resume()
    }
    
    func getWeatherIcon(iconID : String, completion: @escaping (UIImage)->())
    {
        let urlString = "https://openweathermap.org/img/w/" + iconID + ".png"
        DispatchQueue.global().async {
            if let data = try? Data( contentsOf:URL(string: urlString)!)
            {
                if let img = UIImage(data:data)
                {
                    completion(img)
                }
            }
        }
    }
    
}
