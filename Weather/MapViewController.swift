//
//  MapViewController.swift
//  Weather
//
//  Created by brian hickey on 02/04/2019.
//  Copyright © 2019 brian hickey. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation

class MapViewController : UIViewController, MKMapViewDelegate
{
    let mapView = MKMapView()
    var location : CLLocation?
    var annotation : MKPointAnnotation?
    var placeName : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.save))
        self.navigationItem.rightBarButtonItem = saveItem
        
        
        self.title = "Select Location"
        view.addSubview(mapView)
        mapView.delegate = self
        mapView.translatesAutoresizingMaskIntoConstraints = false
        
        let longPressRecogniser = UILongPressGestureRecognizer(target: self, action: #selector(dropPin))
        longPressRecogniser.minimumPressDuration = 0.15
        mapView.addGestureRecognizer(longPressRecogniser)
        
        view.addConstraints([
            NSLayoutConstraint(item: mapView, attribute: .top, relatedBy: .equal,toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .right, relatedBy: .equal,toItem: view, attribute: .right, multiplier: 1, constant: 0)
            ])
    }
    
    @objc func save()
    {
        if let coord = annotation?.coordinate
        {
            var location = Location()
            location.lat = coord.latitude
            location.lon = coord.longitude
            if placeName != nil
            {
                location.title = placeName
            }
            else
            {
                location.title = "Selected Location"
            }
            
            WeatherManager.shared.saveLocation(location: location)
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Warning", message: "No location selected", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Return to main screen", style: .default, handler: { (action) in
                WeatherManager.shared.saveMode(weatherMode: WeatherMode.current)
                self.dismiss(animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Pick a location", style: .cancel, handler: { (action) in
                
            }))
            self.present(alert, animated: true) {
                
            }
        }
    }
    
    func getLocationName()
    {
        if let loc = self.location
        {
            self.getPlace(for: loc) { (placemark) in
                
                if let locality = placemark?.locality
                {
                    self.placeName = locality
                }
            }
        }
    }
    
    func getPlace(for location: CLLocation,
                  completion: @escaping (CLPlacemark?) -> Void) {
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            
            guard error == nil else {
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?[0] else {
                completion(nil)
                return
            }
            
            completion(placemark)
        }
    }
    
    
    @objc func dropPin(gestureRecogniser : UIGestureRecognizer)
    {
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        let point = gestureRecogniser.location(in: mapView)
        let coord = mapView.convert(point, toCoordinateFrom: mapView)
        location = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
        
        annotation = MKPointAnnotation()
        annotation!.coordinate = coord
        mapView.addAnnotation(annotation!)
        self.getLocationName()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
    }

}
