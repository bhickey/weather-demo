//
//  LocationManager.swift
//  Weather
//
//  Created by brian hickey on 02/04/2019.
//  Copyright © 2019 brian hickey. All rights reserved.
//

import Foundation
import CoreLocation


class LocationManager : NSObject, CLLocationManagerDelegate
{
    static let shared = LocationManager()
    
    private var curLocation : CLLocation? = nil
    
    private var locationManager = CLLocationManager()
    
   
    
    
    public var currentLocation : Location?
    {
        guard let coord = self.curLocation else {
            return nil
        }
        
        if (!CLLocationCoordinate2DIsValid(coord.coordinate)) || (coord.coordinate.latitude == 0.0 || coord.coordinate.longitude == 0.0)
        {
            return nil
        }
        
        var loc = Location()
        loc.lat = coord.coordinate.latitude
        loc.lon = coord.coordinate.longitude
        loc.title = "Current Location"
        
        return loc
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.startUpdatingLocation()
    }
    
    
    private func startUpdatingLocation()
    {
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let loc = locations.last
        {
            curLocation = loc
            WeatherManager.shared.loadWeather()
        }
        
    }
    public func permssionDenied()->Bool
    {
        let status = CLLocationManager.authorizationStatus()
        if status == .restricted || status == .denied
        {
            return true
        }
        self.startUpdatingLocation()
        return false
    }
    public func permissionRequest()
    {
        let status = CLLocationManager.authorizationStatus()
        guard status == .notDetermined else {
            self.startUpdatingLocation()
            return }
        locationManager.requestWhenInUseAuthorization()
    }
    
   
    
    
}
