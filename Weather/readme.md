Brian Hickey - Weather demo

1. Build instructions: Shouldn't be any trouble, I haven't used any CocoaPods or other external source. App should run on any device in portrait or landscape, split view and popover on iPad.

2. What else could be done:
    a. Layout. It's obviously quite barebones due to time constraints. I'd have liked to had time to work on a colour scheme, add more data points, graph changes etc and a nicer layout with UICollectionView & columns instead of UITableView. App Icon and launch screen would be nice.
    b. Persistance. I have the mode and location persisting via UserDefaults, but it would have been nice to have the time to persist the actual weather data as well with CoreData. The weather icons should also be cached.
    c. Popups. I've just used UIAlertController to save time but custom views here would have been nice.
    d. Apple Watch Extension. I have experience building these, weather apps are quite suitable to Apple Watch complications.
    e. Location tracking. I just have it calling the loadWeather() on all location changes, but the free openweathermap API limit is 60 requests per hour. I would envisage this being hit very quickly. Some effort to rate limit this would be desirable, such as tracking only significant changes after an initial location lookup.
    f. Comments. I think the project is small enough that it explains itself for the purposes of this exercise, but if I had more time I would have documented with Swift markdown comments.
    g. Map View. I would have a default pin at previously saved location or current user location. 
    h. Current Location title. I would have used the CLGeocoder.reverseGeocodeLocation to look this up. This function in MapViewController should probably be moved to LocationManager as well.
    i. Error handling. App has very limited error handling, a custom popup view could be used to inform user if their phone was disconnected, API was down, etc. But I felt UIAlertController would be too annoying for these types of messages. 
