//
//  Constants.swift
//  allvalue
//
//  Created by brian hickey on 20/03/2018.
//  Copyright © 2018 brian hickey. All rights reserved.
//

import Foundation
import UIKit

class Constants: NSObject
{
    static var margin : CGFloat = 15
    static var tableCellHeight : CGFloat = 100
    static var weatherTableCellIdentifier : String = "WeatherTableViewCell"
  
    static var grey : UIColor = UIColor(white: 0.29, alpha: 1.0)
    static var greyLight : UIColor = UIColor(white: 0.84, alpha: 1.0)
   
    
    class func appFontWithSize(size: CGFloat) -> UIFont
    {
        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.regular)
    }
    
    class func appFontBoldWithSize(size: CGFloat) -> UIFont
    {
        return UIFont.boldSystemFont(ofSize: size)
    }
    
    class func appFontMediumWithSize(size: CGFloat) -> UIFont
    {
        return UIFont.italicSystemFont(ofSize: size)
    }
}




