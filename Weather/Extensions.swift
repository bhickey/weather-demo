//
//  Extensions.swift
//  Weather
//
//  Created by brian hickey on 02/04/2019.
//  Copyright © 2019 brian hickey. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    var dateFromString: Date? {
        
        if let date = Formatter.dateFromStringFormat.date(from: self)
        {
            return date
        }
        return nil
    }
}


extension Formatter {
    
    
    static let prettyDateTime: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE HH:mm"
        return formatter
    }()
    
    static let prettyDay: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter
    }()
    
    static let dateFromStringFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
}


extension Date {

    
    var prettyDateTime: String {

        return Formatter.prettyDateTime.string(from: self)
    }
    
    var prettyDay: String {

        return Formatter.prettyDay.string(from: self)
    }
    
}

