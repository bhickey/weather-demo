//
//  ViewController.swift
//  Weather
//
//  Created by brian hickey on 02/04/2019.
//  Copyright © 2019 brian hickey. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerRows.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerRows[row]
    }
    
    
    enum selectionMode : String
    {
        case all
        case zeroDays
        case oneDays
        case twoDays
        case threeDays
        case fourDays
    }
    
    
    
    
    

    let picker = UIPickerView()
    let pickerView = UIView()
    var pickerYConstraint : NSLayoutConstraint!
    var pickerDone = UIButton()
    var pickerRows = [String]()
    var pickedDay = "5 Days"
    let tableView = UITableView()
    var weathers = [Weather]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerYConstraint = NSLayoutConstraint(item: pickerView, attribute: .bottom, relatedBy: .equal,
                                               toItem: self.view, attribute: .bottom,
                                               multiplier: 1, constant: 1000)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = Constants.tableCellHeight
        tableView.register(WeatherTableViewCell.self, forCellReuseIdentifier: Constants.weatherTableCellIdentifier)
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([
            NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal,toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .right, relatedBy: .equal,toItem: view, attribute: .right, multiplier: 1, constant: 0)
        ])
        
        let settingsItem = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(self.openSettings))
        self.navigationItem.rightBarButtonItem = settingsItem
    
        
        
        pickerView.backgroundColor = Constants.greyLight
        view.addSubview(pickerView)

        picker.dataSource = self
        picker.delegate = self
        pickerView.addSubview(picker)
        
        pickerDone.backgroundColor = .clear
        pickerDone.setTitle("Done", for: .normal)
        pickerDone.setTitleColor(.black, for: .normal)
        pickerDone.addTarget(self, action: #selector(hidePicker), for: .touchUpInside)
        pickerView.addSubview(pickerDone)
        
        pickerDone.translatesAutoresizingMaskIntoConstraints = false
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraints([ NSLayoutConstraint(item: pickerView, attribute: .left, relatedBy: .equal,
                                                 toItem: view, attribute: .left,
                                                 multiplier: 1, constant: 0),
                              NSLayoutConstraint(item: pickerView, attribute: .right, relatedBy: .equal,
                                                 toItem: view, attribute: .right,
                                                 multiplier: 1, constant: 0),
                              NSLayoutConstraint(item: pickerView, attribute: .height, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute,
                                                 multiplier: 1, constant: picker.frame.size.height + 40),
                              pickerYConstraint,
                              
                              
                              NSLayoutConstraint(item: pickerDone, attribute: .top, relatedBy: .equal,
                                                 toItem: pickerView, attribute: .top,
                                                 multiplier: 1, constant: 0),
                              NSLayoutConstraint(item: pickerDone, attribute: .right, relatedBy: .equal,
                                                 toItem: pickerView, attribute: .right,
                                                 multiplier: 1, constant: -Constants.margin),
                              NSLayoutConstraint(item: pickerDone, attribute: .height, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute,
                                                 multiplier: 1, constant: 40),
                              NSLayoutConstraint(item: pickerDone, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute,
                                                 multiplier: 1, constant: 100),
                              
                              NSLayoutConstraint(item: picker, attribute: .top, relatedBy: .equal,
                                                 toItem: pickerView, attribute: .top,
                                                 multiplier: 1, constant: 0),
                              NSLayoutConstraint(item: picker, attribute: .centerX, relatedBy: .equal,
                                                 toItem: pickerView, attribute: .centerX,
                                                 multiplier: 1, constant: 0)
            ])
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name("refresh"), object: nil)

    }
    
    func addPickerItem()
    {
        let pickerItem = UIBarButtonItem(title: pickedDay, style: .plain, target: self, action: #selector(self.showPicker))
        self.navigationItem.leftBarButtonItem = pickerItem
    }
    
    @objc func showPicker()
    {
        
        self.pickerYConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (complete) in
            
        }
    }
    
    @objc func hidePicker()
    {
        self.pickedDay = pickerRows[picker.selectedRow(inComponent: 0)]
        self.refresh()
        self.pickerYConstraint.constant = 1000
        UIView.animate(withDuration: 0.5) {
            
            self.view.layoutIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let weather = weathers[indexPath.row]
        let cell: WeatherTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constants.weatherTableCellIdentifier, for: indexPath) as! WeatherTableViewCell
        
        cell.clean()
        
        
        
        cell.titleLabel.text = weather.text
        if let temp = weather.temp
        {
            cell.tempLabel.text = String(format:"%.1f", temp) + "° C"
        }
        if let date = weather.dateString
        {
            if let dateValue = date.dateFromString
            {
                cell.dateLabel.text = dateValue.prettyDateTime
            }
        }
        
        if let windSpeed = weather.windSpeed
        {
            cell.windLabel.text = "Wind: " + String(format: String(format:"%.1f", windSpeed)) + "KM/H"
        }
        
        if let iconID = weather.icon
        {
            WeatherManager.shared.getWeatherIcon(iconID: iconID) { (img) in
                DispatchQueue.main.async {
                    cell.imgView.image = img
                }
            }
        }
        
        return cell
    }
    
    @objc func refresh()
    {
        
        let allWeathers = WeatherManager.shared.weathers
        
        var selectedWeathers = [Weather]()
        selectedWeathers =  selectedWeathers.sorted(by: { $0.unixDate! < $1.unixDate! })
        
        var days = [String]()
        days.append("5 Days")
        for weather in allWeathers
        {
            if let date = weather.dateString
            {
                if let dateValue = date.dateFromString
                {
                    if(!days.contains(dateValue.prettyDay))
                    {
                        days.append(dateValue.prettyDay)
                    }
                }
            }
        }
        
       pickerRows = days
        
        
        for weather in allWeathers
        {
            if(pickedDay == "5 Days")
            {
                selectedWeathers.append(weather)
            }
            else
            {
                if let date = weather.dateString
                {
                    if let dateValue = date.dateFromString
                    {
                        if(dateValue.prettyDay == self.pickedDay)
                        {
                            selectedWeathers.append(weather)
                        }
                    }
                }
            }
        }
        
        self.weathers = selectedWeathers
        
        DispatchQueue.main.async
            {
                self.addPickerItem()
                self.picker.reloadAllComponents()
                self.title = WeatherManager.shared.currentLocation?.title
                self.tableView.reloadData()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if WeatherManager.shared.currentMode == nil
        {
            self.openSettings()
        }
        
        WeatherManager.shared.loadWeather()
    }

    
    @objc func openSettings()
    {
        let alert = UIAlertController(title: "Settings", message: "Select your weather location", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Current location", style: .default, handler: { (action) in
            WeatherManager.shared.saveMode(weatherMode: WeatherMode.current)
            
            LocationManager.shared.permissionRequest()
            
            if (LocationManager.shared.permssionDenied())
            {
                self.showPermissionAlert()
            }
        }))
        alert.addAction(UIAlertAction(title: "Selected location from map", style: .default, handler: { (action) in
            WeatherManager.shared.saveMode(weatherMode: WeatherMode.saved)
            
            self.present(UINavigationController(rootViewController: MapViewController()), animated: false, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func showPermissionAlert()
    {
        let alert = UIAlertController(title: "Alert", message: "Location Permission denied", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "View Settings", style: .default, handler: { (action) in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        self.present(alert, animated: true) {
            
        }
    }
    
    
    
    
    
}


